import csv
import json
import requests
from bs4 import BeautifulSoup


# Both the House and Senate appear to use the same web template via 'body' flag
house_url = 'https://www.legis.state.pa.us/cfdocs/legis/home/member_information/mbrList.cfm?body=H'
senate_url = 'https://www.legis.state.pa.us/cfdocs/legis/home/member_information/mbrList.cfm?body=S'


def process_directory(url):
    """Given a URL, process the directory and individual biographies of legislators and extract all useful data."""

    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find(class_="MemberInfoList-ListContainer clearfix")

    member_directory = results.find_all('div', {
        'class': "MemberInfoList-MemberWrapper"})
    members = []

    entry_url_base = 'https://www.legis.state.pa.us/cfdocs/legis/home/member_information/'

    for member_entry in member_directory:

        member_record = {}

        # Grab data from member directory first
        entry = member_entry.find("div", class_="MemberInfoList-MemberBio").contents

        name = [name.strip() for name in entry[1].text.split(',')]
        member_record['first_name'] = name[1]
        member_record['last_name'] = name[0]

        if entry[2][2] == 'D':
            member_record['party_affiliation'] = 'Democratic'
        elif entry[2][2] == 'R':
            member_record['party_affiliation'] = 'Republican'

        member_record['bio_url'] = entry_url_base + entry[1].a['href']

        if url[-1] == 'H':
            member_record['body'] = 'House'
        elif url[-1] == 'S':
            member_record['body'] = 'Senate'
        else:
            member_record['body'] = '?'

        # Grab data from full biography page next
        biography = BeautifulSoup(requests.get(member_record['bio_url']).content, "html.parser").find(class_="MemberBio")
        # print(biography.prettify())

        try:
            member_record['photo_thumb_url'] = member_entry.find("div", class_="MemberInfoList-PhotoThumb").img["src"]
        except TypeError:
            member_record['photo_thumb_url'] = ''

        member_record['photo_url'] = biography.aside.img['src']

        try:
            social_profiles = {c.a.span['title'].lower(): c.a['href'] for c in biography.aside.div.find_all("div")}
        except (AttributeError, TypeError, KeyError):
            social_profiles = {}
        # print(social_profiles)

        # Pre-populate social account fields
        member_record['email'] = ''
        member_record['facebook'] = ''
        member_record['twitter'] = ''
        member_record['youtube'] = ''

        # if social_profiles:
        #     for social_account in social_profiles:
        #         print(social_account.i)
        #         if social_account in ['email', 'facebook', 'twitter', 'youtube'] and len(social_account) == 2:
        #             member_record.update(social_account)
        #
        # print(member_record['facebook'])

        contact_info = biography.aside.find_all("address")
        member_record['contact_info'] = contact_info  # Still needs a lot of work

        member_record['old_district'] = entry[4].strip()[9:]

        member_record['new_district'] = 'TBD'  # Do we have this data?

        term_info = str(biography.find(class_="MemberBio-TermInfo").string)
        member_record['term_info'] = term_info.strip()

        member_record['seeking_re-election'] = 'TBD'  # Do we have this data?

        # print(member_record)

        members.append(member_record)

    return members


house_data = process_directory(house_url)
senate_data = process_directory(senate_url)

print(len(house_data))
print(len(senate_data))


with open('pa_house_directory.csv', 'w', newline='') as csvfile:
    fieldnames = ['first_name', 'last_name', 'party_affiliation', 'bio_url',
                  'body', 'photo_thumb_url', 'photo_url', 'email', 'facebook',
                  'youtube', 'twitter', 'contact_info', 'old_district', 'new_district', 'term_info',
                  'seeking_re-election']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(house_data)
    csvfile.close()

#json.dump(house_data, open('pa_house_directory.json', 'w'))

#json.dump(senate_data, open('pa_senate_directory.json', 'w'))
